import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import Viewer from './Component.Viewer/Component.Viewer';
import Sender from './Component.Sender/Component.Sender';
import {createStore} from 'redux'
import http from '../tchatxhr'
import Modal from 'react-modal'
// import './Component.Tchat.css'
const messagesReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_MESSAGE':
    { 
      http({data:action.message,method:'POST'},(xhr)=>{
        if(xhr.status>=400){
          this.setState({isModalOpen:true,erreurCode:xhr.status,erreurText:xhr.statusText})
        }
        
      })
      let obj= [...state, action.message];
      console.log(obj);
      return obj;
    }
    case 'ADD_MESSAGES':
    { 
      let obj= [...state, ...action.messages];
      console.log(obj);
      return obj;
    }
    default:
       return state;
  }
}

let messageStore=createStore(messagesReducer);
class Tchat extends Component {
  constructor(props) {
    super(props);
    this.state = {isModalOpen:false};
  
  http({ressource:'publicDiscussion',method:'GET'},(xhr)=>{

    if(xhr.status>=400){
      this.setState({isModalOpen:true,erreurCode:xhr.status,erreurText:xhr.statusText})
    }
    else{
     messageStore.dispatch({type: 'ADD_MESSAGES', messages: JSON.parse(xhr.response)});
    }
  })
  
  }
  render() {
  //  const {} = this.props;
    return (
      <div>
        <Viewer store={messageStore} />
        <Sender store={messageStore} />  <Modal isOpen={this.state.isModalOpen} contentLabel={"Erreur"+this.state.erreurCode}  >
          {this.state.erreurCode}:{this.state.erreurText}
        </Modal>
        
      </div>
    );
  }
}

Tchat.propTypes = {};

export default Tchat;
