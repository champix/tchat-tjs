import React from 'react';
import './Component.Viewer.css'
// import PropTypes from 'prop-types';

class Viewer extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    this.state = { messages: [{ text: 'value', userId: '' }] };
    this.props.store.subscribe(() => {
      this.setState({ messages: this.props.store.getState() });
      console.log('from store in viewer', this.state.messages)
    })

  }
  render = () => {

    return (
      <div className="viewer-messages">
        {this.state.messages.map((e, i) => {
          let classColor = (i % 2 === 0) ? ' _odd' : ' _even';
          return <div className={"viewer-messages-message" + classColor} key={"viewer-message-" + i}>Message N° {i + 1} : {e.message}<hr /></div>
        })}
      </div>
    );
  }
}

// const Viewer = (props) => { 

//   return (<div ></div>) 

// };

// Viewer.propTypes = {
//   store: PropTypes.string.isRequired,
// };

export default Viewer;
