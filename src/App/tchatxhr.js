const ADR_SRV='http://localhost:666';
/**
 * 
 * @param {Object} params 
 *
 */
export default function http(params,callback) {
      //ressource
      //id * 
      //value by def. if not present
 if(undefined===params)params={};
    let url=
    ADR_SRV+'/publicDiscussions';
    // ADR_SRV+'/'+(undefined!==params&& undefined!==params.ressource)?params.ressource:'publicDiscussions';
    //complete with id if present
    if(undefined !==params &&undefined!==params.id)url+='/'+params.id
    
    let method=(params===undefined || undefined===params.method)
    ?'GET':params.method;
   // let thishttp=this;
    var xhr=new XMLHttpRequest();
    xhr.open(method,url);
    xhr.setRequestHeader('Content-Type','application/json');
    xhr.onreadystatechange=function(response){  
        //appele pour le chargement du store 
        //console.log(thishttp);
        if(xhr.readyState<4)return ;
        console.log(response.target.response);

        callback && callback( response.target);
    }

    xhr.send((undefined !==params&&undefined !== params.data)?JSON.stringify(params.data):undefined)
    
}
// http();